# Generated by Django 2.0.1 on 2018-01-13 16:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0017_auto_20180113_1303'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='personalityalias',
            name='hidden_for',
        ),
        migrations.RemoveField(
            model_name='personalityalias',
            name='is_blacklisted',
        ),
    ]
