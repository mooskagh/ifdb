# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-12-23 16:38
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0008_auto_20171223_1555'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gamevote',
            name='game_finished',
        ),
        migrations.RemoveField(
            model_name='gamevote',
            name='play_time_mins',
        ),
    ]
