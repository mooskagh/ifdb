# Generated by Django 2.0.1 on 2018-01-13 08:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0013_auto_20180112_2152'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gamecomment',
            name='subject',
        ),
    ]
