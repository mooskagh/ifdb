# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-08 12:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0004_auto_20171007_0848'),
    ]

    operations = [
        migrations.AddField(
            model_name='personalityalias',
            name='is_blacklisted',
            field=models.BooleanField(default=False),
        ),
    ]
