# Generated by Django 2.0.1 on 2018-02-04 12:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0004_auto_20180204_1220'),
    ]

    operations = [
        migrations.RenameField(
            model_name='gamelistentry',
            old_name='datetime',
            new_name='date',
        ),
    ]
