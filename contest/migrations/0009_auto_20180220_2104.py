# Generated by Django 2.0.1 on 2018-02-20 21:04

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0008_auto_20180216_2100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='competition',
            name='end_date',
            field=models.DateField(default=datetime.datetime(2018, 2, 20, 21, 4, 38, 409341)),
            preserve_default=False,
        ),
    ]
