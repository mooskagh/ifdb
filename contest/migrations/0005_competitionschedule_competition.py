# Generated by Django 2.0.1 on 2018-01-30 21:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0004_auto_20180130_2113'),
    ]

    operations = [
        migrations.AddField(
            model_name='competitionschedule',
            name='competition',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='contest.Competition'),
            preserve_default=False,
        ),
    ]
