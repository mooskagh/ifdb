# Generated by Django 2.0.1 on 2019-08-03 12:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0013_competitiondocument_order'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='competitionvote',
            name='section',
        ),
        migrations.AddField(
            model_name='competitionvote',
            name='nomination',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='contest.GameList'),
        ),
    ]
