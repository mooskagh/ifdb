# Generated by Django 2.0.1 on 2018-01-28 19:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompetitionDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(blank=True)),
                ('title', models.CharField(max_length=256)),
                ('text', models.TextField()),
                ('view_perm', models.CharField(default='@admin', max_length=256)),
            ],
        ),
        migrations.RemoveField(
            model_name='competition',
            name='description',
        ),
        migrations.AddField(
            model_name='competitiondocument',
            name='competition',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contest.Competition'),
        ),
    ]
