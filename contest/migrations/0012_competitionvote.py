# Generated by Django 2.0.1 on 2018-03-17 15:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0019_auto_20180216_2137'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contest', '0011_auto_20180222_1921'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompetitionVote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('when', models.DateTimeField()),
                ('section', models.CharField(blank=True, max_length=255, null=True)),
                ('field', models.CharField(max_length=255)),
                ('bool_val', models.NullBooleanField()),
                ('int_val', models.IntegerField(blank=True, null=True)),
                ('text_val', models.TextField(blank=True, null=True)),
                ('ip_addr', models.CharField(blank=True, max_length=50, null=True)),
                ('session', models.CharField(blank=True, max_length=32, null=True)),
                ('perm', models.TextField(blank=True, null=True)),
                ('competition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contest.Competition')),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='games.Game')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_permissions': (),
            },
        ),
    ]
