// +build !debug

package main

const (
	OwnPackageName       = "loonchator"
	OwnFileName          = "loonchator.exe"
	OwnVersion           = "0.05.0"
	OwnUrlSchema         = "ersatzplut"
	OwnServerPrefix      = "http://db.mooskagh.com/api/v0/"
	ServerFetchUrlPrefix = "http://db.mooskagh.com/f/packages/"
)
