// +build debug

package main

const (
	OwnPackageName       = "loonchator-debug"
	OwnFileName          = "loonchator-debug.exe"
	OwnVersion           = "0.05.0"
	OwnUrlSchema         = "ersatzplut-debug"
	OwnServerPrefix      = "http://localhost:8000/api/v0/"
	ServerFetchUrlPrefix = "http://localhost:8000/f/packages/"
)
